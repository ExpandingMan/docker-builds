# Expanding Man's Docker Builds
[![docs](https://img.shields.io/badge/docker-registry-blueviolet?style=for-the-badge&logo=julia)](https://gitlab.com/ExpandingMan/docker-builds/container_registry)

This is my repository for storing the build scripts for my docker images.  Much of what is
built in these docker images is done by automated installations from my
[dotfiles](https://gitlab.com/ExpandingMan/dotfiles).

**NOTE to self:** If you need to force re-building, use the `--no-cache` option.  I have
set the build scripts up to accept arguments.

### Building
You can build each of these images with
```
./build.sh
```
This accepts `docker build` options.  By default, images will be built with a tag with the
same name as the enclosing directory.


## Images

### `arch-base`
An almost minimal Arch linux build.  Updates repositories, installs Julia, `git` and a
couple of other things you'll almost always need.  This also includes a bare (default
configured) `nvim` form the package manager (figured I have to have an editor anyway so
might as well).

### `arch-sandbox1-root`
A basic Arch linux build where the main user is `root` using `zsh`, setup with `prezto`
and a fully configured `nvim` according to [my dotfiles](https://gitlab.com/ExpandingMan/dotfiles).

### `arch-sandbox1`
The same thign as `arch-sandbox1-root` but with an additional user `expandingman` set as
the default user.  This user has the same set up as `root` in the previous.
