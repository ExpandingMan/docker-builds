#!/bin/bash
DIR=`dirname $0`
GITLAB_REGISTRY=registry.gitlab.com/expandingman/docker-builds
IMGNAME=`basename $(readlink -f $DIR)`
IMGNAME_GITLAB=$GITLAB_REGISTRY/$IMGNAME

if [[ $1 == "push" ]]; then
    docker push $IMGNAME_GITLAB
    exit
fi

IMGNAME_=$IMGNAME
if [[ $1 == "gitlab" ]]; then
    IMGNAME_=$IMGNAME_GITLAB
    shift 1
fi

docker build -t $IMGNAME_ $* $DIR
